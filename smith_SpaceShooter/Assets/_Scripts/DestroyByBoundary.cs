﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour
{
    void OnTriggerExit(Collider other)
    {
        // Destroy the shots as they leave the scene 
        if (other.tag != "EnterTwo")
        {
            Destroy(other.gameObject);
        }
    }
}
