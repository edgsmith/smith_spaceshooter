﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByTime : MonoBehaviour
{
    // Getting rid of explosions after time 
    public float time;

    void Start()
    {
        Destroy(gameObject, time);
    }
}
