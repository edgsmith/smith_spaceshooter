﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class Boundary
{
    // Keeping player in the field of play 
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    // Setting up player controls
    private Rigidbody rb;
    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    private float nextFire;
    private AudioSource audioSource;

    public GUIText beatGameText;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        beatGameText.text = "";
    }

    void Update()
    {
        // Firing the shots 
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, Quaternion.Euler(shot.transform.rotation.x, shotSpawn.transform.rotation.y, shotSpawn.transform.rotation.z));
            // Play shot sound
            audioSource.Play();
        }

        // Firing the power shots 
        if (Input.GetKeyDown("space") && GameController.bombs > 0)
        {
            Bomber();
            audioSource.Play();
            GameController.bombs -= 1;
        }
    }
    void FixedUpdate()
    {
        // Create verical and horizontal movement of player 
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
        rb.velocity = movement * speed;

        // Constrain the character's movement
        rb.position = new Vector3(Mathf.Clamp(rb.position.x, boundary.xMin, boundary.xMax), 0.0f, Mathf.Clamp(rb.position.z, boundary.zMin, boundary.zMax));

        // Adjust the tilt of the ship 
        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }

    void OnTriggerEnter(Collider other)
    {
        // Give player bombs for collecting power up 
        if (other.tag == "Power")
        {
            GameController.bombs += 1;
            Destroy(other.gameObject);
        }

        // Jump to scene two when swarm has ended 
        if (other.tag == "EnterTwo")
        {
            SceneManager.LoadScene("SceneTwo");
        }
        
        // Continue scene jumping until end 
        if (other.tag == "EnterThree")
        {
            SceneManager.LoadScene("SceneThree");
        }

        if (other.tag == "EnterFour")
        {
            SceneManager.LoadScene("SceneFour");
        }

        if (other.tag == "BeatGame")
        {
            beatGameText.text = "You beat the game!";
        }
    }

    void Bomber() 
    {
        // Spawn 90 super shots 
        for (int i = 1; i <= 89; i++)
        {
            Instantiate(shot, shotSpawn.position, Quaternion.Euler(shot.transform.rotation.x, -i, shot.transform.rotation.z));
        }

        for (int i = 1; i <= 89; i++)
        {
            Instantiate(shot, shotSpawn.position, Quaternion.Euler(shot.transform.rotation.x, i, shot.transform.rotation.z));
        }
        Instantiate(shot, shotSpawn.position, Quaternion.Euler(shot.transform.rotation.x, 0, shot.transform.rotation.z));
    }
    // Starting new levels create a new scene and add the scene in the build settings  
    /* OnTriggerEnter
     * if player hits the trigger 
     *      SceneManager.LoadScene("Scene's name");
     *      
    */

    // Pickup spawning 
    /* GameObjects[] pickupPrefabs
     * Start()
     *      call SpawnPickup()
     * 
     * SpawnPickup()
     *      GameObject toSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
     *      instantiate(toSpawn, transform.position, toSpawn.transform.rotation);
    */
}