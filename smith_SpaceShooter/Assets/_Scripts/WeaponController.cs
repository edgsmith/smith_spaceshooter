﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;
    public float delay;

    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", delay, fireRate);
    }

    // Allow enemies to fire shots 
    void Fire()
    {
        Instantiate(shot, shotSpawn.position, Quaternion.Euler(shot.transform.rotation.x, shotSpawn.transform.rotation.y, shotSpawn.transform.rotation.z));
        audioSource.Play();
    }
}
