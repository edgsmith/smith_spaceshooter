﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    // Setting up explosions with collisions 
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "EnterTwo")
        {
            if (other.tag != "Power")
            {
                // Destroy if a shot collides with an asteroid 
                if (other.CompareTag("Boundary") || other.CompareTag("Enemy"))
                {
                    return;
                }
                if (explosion != null)
                {
                    Instantiate(explosion, transform.position, transform.rotation);
                }
                if (other.tag == "Player")
                {
                    Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
                    // End the game 
                    gameController.GameOver();
                }
                // Incrementing points 
                gameController.AddScore(scoreValue);
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
