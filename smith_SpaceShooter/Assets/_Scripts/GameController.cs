﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public GameObject powerUp;
    public GameObject sceneLoader;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public GUIText scoreText;
    private int score;
    public static int bombs;

    // Game controlling texts and booleans 
    public GUIText restartText;
    public GUIText gameOverText;
    public GUIText bombText;
    private bool gameOver;
    private bool restart;

    void Start()
    {
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        bombText.text = "Power Shots: 0";
        bombs = 0;
        score = 0;
        UpdateScore();
        // Start the wave 
        StartCoroutine(SpawnWaves());
    }

    void Update()
    {
        // Start game over if desired 
        if (restart)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

        bombText.text = "Power Shots: " + bombs.ToString();
    }

    // Create new hazards randomly in the scene  
    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            // Avoid overlapping spawns with hazards and a power up
            for (int i = 0; i < 0.5 * hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Instantiate(hazard, spawnPosition, transform.rotation);
                yield return new WaitForSeconds(spawnWait);
            }

            Vector3 powerSpawn = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
            Quaternion powerRotation = Quaternion.identity;
            Instantiate(powerUp, powerSpawn, powerUp.transform.rotation);

            for (int i = (int)(0.5 * hazardCount); i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Instantiate(hazard, spawnPosition, transform.rotation);
                yield return new WaitForSeconds(spawnWait);
            }

            Vector3 spawnSceneLoader = new Vector3(6, 0, -16);
            Instantiate(sceneLoader, spawnSceneLoader, Quaternion.identity);

            yield return new WaitForSeconds(waveWait);

            // Check for restarting 
            if (gameOver)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    // Keep track of the score as hazards are destroyed 
    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    // Used to end the game 
    public void GameOver()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
}
