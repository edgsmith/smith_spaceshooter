﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    // Setting up game shots
    private Rigidbody rb;
    public float speed;

    void Start ()
    {
        // Moving shots 
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
    }
}
