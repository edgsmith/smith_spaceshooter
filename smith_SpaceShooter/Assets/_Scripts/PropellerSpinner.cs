﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropellerSpinner : MonoBehaviour
{
    public float speed;


    void Update()
    {
        // Spin the propeller 
        transform.Rotate(Vector3.forward, speed * Time.deltaTime);
    }
}
